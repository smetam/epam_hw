def collatz_steps(n, steps=0):
    return steps if n == 1 else collatz_steps(n // 2, steps + 1) \
        if n % 2 == 0 else collatz_steps(n * 3 + 1, steps + 1)


i = 27
print(i, collatz_steps(i))
