from functools import reduce


def is_armstrong(n):
    s = str(n)
    l = len(s)
    res = reduce(lambda x, y: x + y, [int(x) ** l for x in s])
    return res == n


for i in range(1000):
    if is_armstrong(i):
        print(i, '- Число Армстронга')


