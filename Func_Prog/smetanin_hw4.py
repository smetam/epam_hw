from functools import reduce

problem6 = reduce(lambda x, y: x + y, range(1, 101)) ** 2 - \
           reduce(lambda x, y: x + y, [x * x for x in range(1, 101)])


problem9 = reduce(lambda x, y: x * y, list(filter(
    lambda l: l[0] ** 2 + l[1] ** 2 == l[2] ** 2,
    [(1000 - c - b, b, c) for c in range(1, 999)
     for b in range((1000 - c) // 2, 1000 - c)]))[0])


s = [x for i in range(200000) for x in str(i)]
d = {k: v for k, v in enumerate(s)}

problem40 = reduce(lambda x, y: x * y,
                   [int(d[i]) for i in [10 ** i for i in range(1, 7)]])


problem48 = str(reduce(lambda x, y: x + y,
                       map(lambda x: x,
                           [i ** i for i in range(1, 1001)])))[-10:]


print(problem6)
print(problem9)
print(problem40)
print(problem48)
