from functools import reduce


s1 = reduce(lambda x, y: x + y, map(lambda x: x * x, range(1000 + 1)))
s2 = reduce(lambda x, y: x + y, filter(
    lambda x: x ** 0.5 % 1 == 0, range(10 ** 6 + 1)))
s3 = reduce(lambda x, y: x + y, [i * i for i in range(1000 + 1)])

print(s1, s2, s3)
