import socket
import time
import threading as thr


def receive():
    """Handles receiving of messages."""
    while True:
        try:
            msg = c.recv(512).decode("utf8")
            print(msg)
        except OSError:  # Possibly client has left the chat.
            break


c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c.connect(('localhost', 8000))
receive_thread = thr.Thread(target=receive)
receive_thread.start()

while True:
    data = bytes(input(), 'utf-8')
    c.send(data)
    if data == bytes('/quit', 'utf-8'):
        c.close()
        break

    time.sleep(1)
