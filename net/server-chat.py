from socket import *
import threading as thr


def broadcast(message, pre=''):
    for user in clients:
        clients[user].send(bytes(bytes(pre + message, "utf8")))


def private(msg, name):
    i = msg.find(' ')
    m = msg[1:i]
    if m in clients:
        clients[m].send(bytes(f'Private from {name}: {msg[i:]}', 'utf-8'))
    else:
        clients[name].send(bytes('Incorrect username', 'utf-8'))


def client_thread(sock):
    name = sock.recv(512).decode("utf8")
    while name in clients:
        sock.send(bytes('Name already exists', 'utf-8'))
        name = sock.recv(512).decode("utf8")
    welcome = f'Welcome {name}! To exit type "/quit".\n' \
              f'Type /users to see active users.\n' \
              f'Type @name to send private message '
    sock.send(bytes(welcome, "utf8"))
    msg = "%s has joined the chat!" % name
    broadcast(msg)
    clients[name] = sock

    while True:
        msg = sock.recv(512).decode('utf-8')
        print(msg)
        if msg == "/quit":
            sock.send(bytes("/quit", "utf8"))
            sock.close()
            del clients[name]
            global active
            active -= 1
            broadcast(f"{name} has left the chat.")
            break
        elif msg == '/users':
            for user in clients:
                sock.send(bytes(user + '\n', 'utf-8'))
        elif msg.startswith('@'):
            private(msg, name)
        else:
            broadcast(msg, name + ": ")


def accept_connections():
    while True:
        client_socket, client_address = s.accept()
        print(f"{client_socket}:{client_address} connected.")
        client_socket.send(bytes(
            "Hi, please, enter your name",
            "utf8"))
        addresses[client_socket] = client_address
        thr.Thread(target=client_thread, args=(client_socket,)).start()


clients = {}
addresses = {}
active = 0

s = socket(AF_INET, SOCK_STREAM)
s.bind(('localhost', 8000))


if __name__ == "__main__":
    s.listen(5)
    print("Waiting for connection...")
    accepter = thr.Thread(target=accept_connections)
    accepter.start()
    accepter.join()
    s.close()
