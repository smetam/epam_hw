def atom(value):
    atom_value = value

    def get_value():
        return atom_value

    def set_value(value):
        nonlocal atom_value
        atom_value = value
        return atom_value

    def process_value(*args):
        nonlocal atom_value
        for func in args:
            atom_value = func(atom_value)
        return atom_value

    return get_value, set_value, process_value
