def make_it_count(func, counter_name):
    def up_func(*args, **kwargs):
        nonlocal counter_name
        var = globals()
        var[counter_name] += 1
        return func(*args, **kwargs)
    return up_func
