import inspect


def partial(func, *fixated_args, **fixated_kwargs):
    def up_func(*new_args, **new_kwargs):
        updated_args = list()
        updated_args.extend(fixated_args)
        updated_args.extend(new_args)
        updated_kwargs = fixated_kwargs or {}
        updated_kwargs.update(**new_kwargs)
        return func(*updated_args, **updated_kwargs)

    up_func.__name__ = "partial_" + func.__name__
    doc = "A partial implementation of " + func.__name__ \
          + " with pre-applied arguments being:"
    arg_names = inspect.getfullargspec(func)
    for i in range(len(fixated_args)):
        doc = doc + "\n" + arg_names.args[i] + " = " + str(fixated_args[i])
    for key, value in fixated_kwargs.items():
        doc = doc + "\n" + str(key) + " = " + str(value)
    doc = doc + "."
    up_func.__doc__ = doc
    return up_func
