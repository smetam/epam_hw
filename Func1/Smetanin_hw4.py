def letters_range(*args):
    res = []
    if len(args) == 1:
        start = ord('a')
        stop = ord(args[0])
        step = 1
    elif len(args) == 2:
        start = ord(args[0])
        stop = ord(args[1])
        step = 1
    elif len(args) == 3:
        start = ord(args[0])
        stop = ord(args[1])
        step = args[2]
    else:
        print("letters_range(start='a', stop, step=1)")
        return []
    for i in range(start, stop, step):
        res.append(chr(i))
    return res
