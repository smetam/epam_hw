def min_max(*args):
    m = sorted(args)
    return m[0], m[len(args) - 1]


def count_points(a, b, c):
    res = 0
    x1, y1 = a
    x2, y2 = b
    x3, y3 = c
    min_x, max_x = min_max(a[0], b[0], c[0])
    min_y, max_y = min_max(a[1], b[1], c[1])
    for x0 in range(min_x, max_x + 1):
        for y0 in range(min_y, max_y + 1):
            r1 = (x1 - x0) * (y2 - y1) - (x2 - x1) * (y1 - y0)
            r2 = (x2 - x0) * (y3 - y2) - (x3 - x2) * (y2 - y0)
            r3 = (x3 - x0) * (y1 - y3) - (x1 - x3) * (y3 - y0)
            if r1 <= 0 and r2 <= 0 and r3 <= 0:
                res += 1

            elif r1 >= 0 and r2 >= 0 and r3 >= 0:
                res += 1

    return res


print(count_points((-2, -5), (0, 0), (5, 2)))
print(count_points((5, 2), (0, 0), (-2, -5)))
print(count_points((5, 2), (-2, -5), (0, 0)))
