import time as tm
import math
import numpy as np
from numpy import linalg as la
import sys


def make_track(global_var):

    def decorator(f):

        def wrapper(*args):
            g = globals()
            track = g[global_var]
            track['used'] += 1
            t = tm.time()
            val = f(*args)
            track['time'] += tm.time() - t
            return val

        return wrapper
    return decorator


TRACK_FIB_REC = {'used': 0, 'time': 0}
TRACK_FIB_ENC = {'used': 0, 'time': 0}
TRACK_FIB_MAT = {'used': 0, 'time': 0}
TRACK_FIB_CACHE = {'used': 0, 'time': 0}
TRACK_FIB_DYN = {'used': 0, 'time': 0}


@make_track('TRACK_FIB_REC')
def fib_rec(n):
    if n < 2:
        return n
    return fib_rec(n - 1) + fib_rec(n - 2)


@make_track('TRACK_FIB_ENC')
def fib_enc(n):
    sqrt5 = math.sqrt(5)
    phi = (sqrt5 + 1) / 2
    return int(phi ** n / sqrt5 + 0.5)


M = {0: 0, 1: 1}


@make_track('TRACK_FIB_CACHE')
def fib_cache(n):
    if n in M:
        return M[n]
    M[n] = fib_cache(n - 1) + fib_cache(n - 2)
    return M[n]


@make_track('TRACK_FIB_DYN')
def fib_dyn(n):
    a = 0
    b = 1
    for i in range(n):
        a, b = b, a + b
    return a


def matrix_multiply(A, B):
    BT = list(zip(*B))
    return [[sum(a * b
                 for a, b in zip(row_a, col_b))
            for col_b in BT]
            for row_a in A]


def matrix_pow(x, k):
    if k == 0:
        return np.identity(2)
    elif k == 1:
        return x
    else:
        y = matrix_pow(x, k // 2)
        y = matrix_multiply(y, y)
        if k % 2:
            y = matrix_multiply(x, y)
        return y


@make_track('TRACK_FIB_MAT')
def fib_mat(n):
    m = [[1, 1], [1, 0]]
    f = matrix_pow(m, n)
    return f[0][1]


u = 4000
sys.setrecursionlimit(10000)

# print('Рекурсия с кэшем:', fib_cache(u), TRACK_FIB_CACHE)
# print('Замкнутая формула:', fib_enc(u), TRACK_FIB_ENC)
# print('Динамический способ:', fib_dyn(u), TRACK_FIB_DYN)
# print('Матричный способ:', fib_mat(u), TRACK_FIB_MAT)
for i in [10, 100, 500, 1000, 2000, 5000]:
    TRACK_FIB_MAT = {'used': 0, 'time': 0}
    TRACK_FIB_CACHE = {'used': 0, 'time': 0}
    TRACK_FIB_DYN = {'used': 0, 'time': 0}

    fib_cache(i)
    fib_dyn(i)
    fib_mat(i)

    print(i)
    print('Рекурсия с кэшем:', TRACK_FIB_CACHE)
    print('Динамический способ:', TRACK_FIB_DYN)
    print('Матричный способ:', TRACK_FIB_MAT)

"""из представленных методов подсчета чисел фиббоначи
 наилучшие результаты дает динамичесеский способ fib_dyn при n < 1000
 если n > 1000, то оптимальным является матричный метод fib_mat"""
