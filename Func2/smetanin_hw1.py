def to_int(s, i=0):
    if len(s) == 0:
        return i
    elif s[0].isdigit():
        return to_int(s[1:], 10 * i + int(s[0]))
    else:
        return None


inp = input()
while inp != 'cancel':
    if inp[0] == '-':
        t = to_int(inp[1:])
        if t is not None:
            t = -1 * t
    else:
        t = to_int(inp)
    if t:
        if t % 2 == 0:
            print(t // 2)
        else:
            print(3 * t + 1)
    else:
        print('Не удалось преобразовать введенный текст в число.')
    inp = input()
print('Bye!')
