import math


def mysqrt(a, e=None, b=1):
    if a == 0:
        return 0
    if e is None:
        e = a if a < 0.01 else 0.01
    if abs(b - a / b) > e / 3:
        return mysqrt(a, e, 1 / 2 * (b + a / b))
    else:
        return round(b, -1 * round(math.log(e, 10)))



