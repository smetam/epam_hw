import time as tm


def make_cache(timer=10):

    def decorator(f):
        cache = {}
        cache_reset_time = tm.time() + timer

        def wrapper(*args):
            nonlocal cache_reset_time
            if tm.time() > cache_reset_time:
                nonlocal cache
                cache.clear()
                cache_reset_time = tm.time() + timer

            if args in cache:
                return cache[args]
            else:
                val = f(*args)
                cache[args] = val
                return val

        return wrapper
    return decorator


@make_cache()
def fib(n):
    if n < 2:
        return n
    return fib(n - 1) + fib(n - 2)


print(fib(100))
