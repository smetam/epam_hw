from oop import smetanin_calcs as clc


class AbstractOptimiser:
    def process(self, expr):
        g = self.pre_process(expr)

        result = self.process_internal(g)

        return self.post_process(result)

    def pre_process(self, expr):
        return str(clc.Calculator(expr))

    def process_internal(self, graph):
        return graph

    def post_process(self, result):
        return str(clc.Calculator(result)).split()


class DoubleNegativeOptimiser(AbstractOptimiser):
    # -(-a) -> a
    def process_internal(self, expr):
        res = []
        for token in expr:
            if token == '~':
                a = res.pop()
                if a != '~':
                    res.append(a)
                    res.append(token)
            else:
                res.append(token)
        return res


class IntegerConstantsOptimiser(AbstractOptimiser):
    # a + 4*2 -> a + 8
    def process_internal(self, expr):
        op = {'-', '+', '*', '/', '^'}
        stack = []
        for token in expr:
            if token.isdigit():
                stack.append((token, 'd'))
            elif token == '~':
                stack.append((token, 'm'))
            elif token in op:
                if stack[-1][1] in ['d', 'm'] and stack[-2][1] in ['d', 'm']:
                    op2 = stack.pop()[0]
                    if op2 == '~':
                        temp = stack.pop()[0]
                        op2 = '-' + temp
                    op1 = stack.pop()[0]
                    if op1 == '~':
                        temp = stack.pop()[0]
                        op1 = '-' + temp
                    if token == '^':
                        res = int(op1) ** int(op2)
                    else:
                        res = int(eval(op1 + token + op2))
                    if res < 0:
                        stack.append((str(res * (-1)), 'd'))
                        stack.append(('~', 'm'))
                    else:
                        stack.append((str(res), 'd'))
                else:
                    stack.append((token, 'o'))
            else:
                stack.append((token, 'v'))
        result = []
        for el in stack:
            result.append(str(el[0]))
        return result


class SimplifierOptimiser(AbstractOptimiser):
    # a * 0 -> 0
    # a + 0 -> a
    def process_internal(self, expr):
        op = {'-', '+', '*', '/', '^'}
        stack = []
        for token in expr:
            if token.isdigit():
                stack.append(token)
            elif token in op:
                op2 = stack.pop()[0]
                op1 = stack.pop()[0]
                if token == '-' and op1 == op2:
                    stack.append('0')
                elif token == '/' and op1 == op2:
                    stack.append('1')
                elif token == '+':
                    if op1 == '0':
                        stack.append(op2)
                    elif op2 == '0':
                        stack.append(op1)
                    else:
                        stack.append(op1)
                        stack.append(op2)
                        stack.append(token)
                elif token == '*':
                    if op1 == '0':
                        stack.append('0')
                    elif op2 == '0':
                        stack.append('0')
                    elif op1 == '1':
                        stack.append(op2)
                    elif op2 == '1':
                        stack.append(op1)
                    else:
                        stack.append(op1)
                        stack.append(op2)
                        stack.append(token)
                elif token == '^':
                    if op1 == '1':
                        stack.append('1')
                    if op2 == '0':
                        stack.append('1')
                    elif op2 == '1':
                        stack.append(op1)
                    else:
                        stack.append(op1)
                        stack.append(op2)
                        stack.append(token)
                else:
                    stack.append(op1)
                    stack.append(op2)
                    stack.append(token)
            else:
                stack.append(token)
        result = ''
        for el in stack:
            result += str(el[0])
        return result.strip()


integer_constant_optimiser_tests = [
    (['1'], ['1']),
    (['1', '+', '2'], ['3']),
    (['1', '-', '2'], ['1 -']),
    (['2', '*', '2'], ['4']),
    (['2', '/', '2'], ['1']),
    (['2', '^', '10'], ['1024']),
    (['a', '+', '2', '*', '4'], ['a 8 +', '8 a +']),
]

for case, exp in integer_constant_optimiser_tests:
    calc = clc.Calculator(case, [IntegerConstantsOptimiser()])

    calc.optimise()

    if str(calc) not in exp:
        print(f'Error in CONSTANT case for "{case}". '
              f'Actual "{calc}", expected one of {exp}')

double_negate_tests = [
    ('-(-a)', 'a'),
    ('-(-5)', '5'),
    ('-(a+b)+c-(-d)', 'a b + - c + d - -'),
]

for case, exp in double_negate_tests:
    tokens = list(case)
    calc = clc.Calculator(tokens, [DoubleNegativeOptimiser()])
    calc.optimise()

    if str(calc) != exp:
        print(f'Error in NEGATE case for "{case}".'
              f' Actual "{calc}", expected {exp}')


# test cases помеченные (*) не обязательны к прохождению.
simplifier_optimiser_test = [
    ('a+0', ['a']),
    ('a*1', ['a']),
    ('a*0', ['0']),
    ('b/b', ['1']),
    ('a-a', ['0']),
    ('a+(b-b)', ['a']),
    ('a+(7-6-1)', ['a']),
    ('a^0', ['1']),
    ('a-(-(-a))', ['0'])
]

for case, exps in simplifier_optimiser_test:
    tokens = list(case)
    calc = clc.Calculator(tokens,
                      [DoubleNegativeOptimiser(), IntegerConstantsOptimiser(),
                       SimplifierOptimiser()])

    calc.optimise()

    if str(calc) not in exps:
        print(f'Error in SIMPLIFIER case for "{case}".'
              f' Actual "{calc}", expected one of {exps}')
