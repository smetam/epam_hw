class Calculator:
    def __init__(self, opcodes: list, operators=None):
        self.opcodes = opcodes
        self.polish_opcodes = self.polish_not()
        self.operators = operators if operators is not None else []
        self.vars = 0
        self.op = 0

    def __str__(self) -> str:
        s = ''
        for el in self.polish_opcodes:
            if el == '~':
                s += '- '
            else:
                s += el + ' '
        return s.strip()

    def polish_not(self):
        expr = list('(')
        for el in self.opcodes:
            expr.append(el)
        expr.append(')')
        stack = []
        mas = ''
        for i in range(len(expr)):
            sym = expr[i]
            if sym.isdigit():
                mas += sym + ' '
            elif sym == '(':
                stack.append(sym)
            elif sym == ')':
                a = stack.pop()
                while a != '(':
                    # if a == '~':
                    #    mas += '- '
                    # else:
                    mas += a + ' '
                    a = stack.pop()
            elif sym == '^':
                stack.append(sym)
            elif sym == '/' or sym == '*':
                a = stack.pop()
                # if a == '~':
                #    mas += '- '
                #    a = stack.pop()
                while a == '/' or a == '*' or a == '^':
                    mas += a + ' '
                    a = stack.pop()
                    # if a == '~':
                    #    mas += '- '
                    #    a = stack.pop()
                stack.append(a)
                stack.append(sym)
            elif sym == '-' or sym == '+':
                if expr[i - 1] in ['(', '-', '+', '*', '/', '^']:
                    stack.append('~')
                else:
                    a = stack.pop()
                    # if a == '~':
                    #    mas += '- '
                    #    a = stack.pop()
                    while a != '(':
                        mas += a + ' '
                        a = stack.pop()
                        # if a == '~':
                        #    mas += '- '
                        #    a = stack.pop()
                    stack.append(a)
                    stack.append(sym)
            else:
                mas += sym + ' '
            mas.strip()
        return mas.split()

    def optimise(self):
        expr = self.polish_opcodes
        for operator in self.operators:
            expr = operator.process_internal(expr)
        self.polish_opcodes = expr

    def validate(self) -> bool:
        # 1 bracket check
        balance = 0
        for sym in self.opcodes:
            if sym == '(':
                balance += 1
            elif sym == ')':
                if balance > 0:
                    balance -= 1
                else:
                    return False
        if balance != 0:
            return False

        # 2 calc graph
        expr = list('(')
        for el in self.opcodes:
            expr.append(el)
        expr.append(')')
        prev_token = '*'
        for token in expr:
            if prev_token == '(':
                if token in ['+', '*', '/', '^']:
                    return False
            elif prev_token in ['-', '+', '*', '/', '^']:
                if token in ['+', '*', '/', '^', ')']:
                    return False
            elif prev_token == ')':
                if token not in ['-', '+', '*', '/', '^', ')']:
                    return False
            else:
                if token not in ['-', '+', '*', '/', '^', ')']:
                    return False
            prev_token = token

        # 3 division by zero check
        if ('/', '0') in zip(self.opcodes, self.opcodes[1:]):
            return False

        return True


validate_check_list = [
    ('a+2', True),
    ('a-(-2)', True),
    ('a+2-', False),
    ('a+(2+(3+5)', False),
    ('a^2', True),
    ('a^(-2)', True),
    ('-a-2', True),
    ('6/0', False),
    ('a/(b-b)', True),  # мы же не знаем, что b-b это 0, увы
    ]

for case, exp in validate_check_list:
    tokens = list(case)

    calc = Calculator(tokens).validate()

    if calc != exp:
        print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

str_check_list = [
    ("a", "a"),
    ("(-a)", "a -"),
    ("(a*(b/c)+((d-f)/k))", "a b c / * d f - k / +"),
    ("(a)", "a"),
    ("a*(b+c)", "a b c + *"),
    ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "a b c / * d f - k / + h g r - * *"),
    ("(x*y)/(j*z)+g", "x y * j z * / g +"),
    ("a-(b+c)", "a b c + -"),
    ("a/(b+c)", "a b c + /"),
    ("a^(b+c)", "a b c + ^"),
    ('a*b/c/d', 'a b * c / d /'),
    ('a*b^c/d', 'a b c ^ * d /'),
    ('a*b^c^d', 'a b c d ^ ^ *'),
    ('-a*(-b*c*(-d))', 'a b c * d - * - * -'),
    ('-(a-b)', 'a b - -'),
    ('a-(-b)', 'a b - -'),
    ('-a^(-b)', 'a b - ^ -')
    ]

for case, exp in str_check_list:
    tokens = list(case)
    calc = Calculator(tokens)

    if str(calc) != exp:
        print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')
