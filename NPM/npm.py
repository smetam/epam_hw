import numpy as np
import time
import pandas as pd
import matplotlib.pyplot as plt


def mk_data(f, t, step):
    data = {'N': [], 'time': []}
    for i in range(f, t, step):
        m1 = np.random.rand(i, i)
        m2 = np.random.rand(i, i)
        # m1 = np.ones((i, i))
        # m2 = np.ones((i, i))
        t0 = time.time()
        m = m1 * m2
        t1 = time.time()
        data['N'].append(i)
        data['time'].append(t1 - t0)
    return data


def mk_dep(df, dif):
    b1 = np.array(df['N'][dif:])
    b2 = np.array(df['N'][:-1 * dif])  # b2 = df['n'][0]
    xd1 = b1
    xd2 = b1 / b2
    c1 = np.array(df['time'][dif:])
    c2 = np.array((df['time'][:-1 * dif]))  # c2 = (df['time'][0])
    yd2 = c1 / c2
    yd1 = np.log(yd2) / np.log(xd2)
    yerrd1 = yd1.std()
    n = yd1.mean()
    return xd1, yd1, yerrd1, n


if __name__ == '__main__':
    # numpy part
    data = mk_data(50, 5000, 300)

    # numpy part for low N
    data_low = mk_data(1, 50, 3)

    # pandas part
    df = pd.DataFrame(data)
    df_low = pd.DataFrame(data_low)
    print(df)
    print('\nMEAN:\n\n', df.mean())
    print('\nSTD:\n\n', df.std())

    # matplotlib part
    x = np.log(df['N'])
    y = np.log(df['time'])
    yerr = np.log(df['time'].std())

    x1, y1, yerr1, n = mk_dep(df, 7)

    print('n: ', n)

    # matplotlib part for low N
    x_low = np.log(df_low['N'])
    y_low = np.log(df_low['time'])
    yerr_low = np.log(df_low['time'].std())

    x1_low, y1_low, yerr1_low, n_low = mk_dep(df_low, 7)

    print('n_low: ', n_low)

    # plot part
    plt.figure(1)
    plt.title('Numpy matrix multiplication stats')
    plt.xlabel('log(N)')
    plt.ylabel('log(Time)')
    plt.errorbar(x, y, yerr=yerr, fmt='o', ecolor='red')
    plt.plot(x, [n * (s - x[len(x) // 2]) + y[len(x) // 2] for s in x])
    plt.grid()
    plt.savefig('fig1.png', format='png', dpi=100)
    plt.savefig('fig1.eps', format='eps', dpi=100)

    plt.figure(2)
    plt.title('n from N')
    plt.xlabel('N')
    plt.ylabel('n')
    plt.errorbar(x1, y1, yerr=yerr1, fmt='o', ecolor='red')
    plt.plot(x1, [n for s in x1])
    plt.grid()
    plt.savefig('fig2.png', format='png', dpi=100)
    plt.savefig('fig2.eps', format='eps', dpi=100)

    # plot part for low N
    plt.figure(3)
    plt.title('Matrix mult stats for N < 50')
    plt.xlabel('log(N)')
    plt.ylabel('log(Time)')
    plt.errorbar(x_low, y_low, yerr=yerr_low, fmt='o', ecolor='red')
    t = len(x_low) // 2
    plt.plot(x_low, [n_low * (s_low - x_low[t]) + y_low[t] for s_low in x_low])
    plt.grid()
    plt.savefig('fig3.png', format='png', dpi=100)
    plt.savefig('fig3.eps', format='eps', dpi=100)

    plt.figure(4)
    plt.title('n from N for N < 50')
    plt.xlabel('N')
    plt.ylabel('n')
    plt.errorbar(x1_low, y1_low, yerr=yerr1_low, fmt='o', ecolor='red')
    plt.plot(x1_low, [n_low for s_low in x1_low])
    plt.grid()
    plt.savefig('fig4.png', format='png', dpi=100)
    plt.savefig('fig4.eps', format='eps', dpi=100)

    plt.show()
