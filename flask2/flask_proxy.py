from flask import Flask, render_template, redirect, url_for
import click
import requests
from bs4 import BeautifulSoup as soup


app = Flask(__name__)
LIMIT = 10
PATH = 'https://www.bbc.com/news'


def fetch_page(path):
    res = requests.get(path)
    res.raise_for_status()
    return res.text


# def mk_tm(html, limit=10):
#    soup = soup(html, 'lxml')


def mk_tm2(word, limit=10):
    if len(word) > limit:
        word += u"\u2122"
    return word

@app.cli.command()
#@app.cli.option('--limit', default=1, help='Number of letters before trademark.')
#@app.cli.option('--url', default='https://www.bbc.com/news',
#              help='The webpage to modify.')
@app.route('/')
def repl():
    text = fetch_page(PATH)
    sp = soup(text, 'lxml')
    for i in sp.find_all():
        if i.string and i.name != 'script':
            s = i.string.split()
            new_s = ""
            for word in s:
                if u"\u2122" not in word and len(word) >= LIMIT:
                    new_s += word + u"\u2122" + ' '
                else:
                    new_s += word + ' '
            i.string.replace_with(new_s)
    return sp.prettify()


@click.command()
@click.option('--limit', default=10, help='Number of letters before trademark.')
@click.option('--path', default='https://www.bbc.com/news',
              help='The webpage to modify.')
@click.option('--port', default=8000,
            help='Port.')
def modify(limit, path, port):
    global LIMIT
    LIMIT = limit
    global PATH
    PATH = path
    app.run(port=port)


if __name__ == '__main__':
    modify()
