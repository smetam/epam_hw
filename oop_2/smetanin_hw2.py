class EnumMeta(type):
    def __new__(mcs, cls, bases, dct):
        _members_ = {k: dct[k] for k in dct if not k.startswith('_')}
        _val2member_ = {dct[k]: k for k in dct if not k.startswith('_')}
        dct.update({'_members_': _members_})
        dct.update({'_val2member_': _val2member_})
        enum_class = super().__new__(mcs, cls, bases, dct)
        inst = {}
        for key in _members_:
            inst[key] = enum_class(_members_[key])
            setattr(enum_class, key, inst[key])
        enum_class._inst_ = inst
        return enum_class

    def __iter__(cls):
        for key, val in cls._inst_.items():
            yield val

    def __getitem__(cls, item):
        if item in cls._inst_:
            return cls._inst_[item]
        else:
            raise KeyError


class Enum(metaclass=EnumMeta):
    def __new__(cls, data):
        if data not in cls._members_ and data not in cls._val2member_:
            raise ValueError
        elif cls._val2member_[data] in cls._inst_:
            return cls._inst_[cls._val2member_[data]]
        elif data in cls._inst_:
            return cls._inst_[data]
        elif data in cls._members_:
            name = data
            value = cls._members_[data]
            e = object.__new__(cls)
            e.__class__ = cls
            e.__name__ = name
            e.value = value
            return e
        else:
            name = cls._val2member_[data]
            value = data
            e = object.__new__(cls)
            e.__class__ = cls
            e.__name__ = name
            e.value = value
            return e

    def __str__(self):
        return "<%s.%s: %s>" % (self.__class__.__name__,
                                self.__name__, self.value)


class Direction(Enum):
    north = 0
    east = 90
    south = 180
    west = 270


print(Direction.__dict__)
for i in Direction:
    print(i)
    print(id(i))

print(id(Direction.north))
print(id(Direction(0)))
