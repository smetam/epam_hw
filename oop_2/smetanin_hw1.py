class PositiveDescriptor:
    label = 'PositiveValue'

    def __init__(self, name):
        self.name = name
        self.val = {}

    def __get__(self, instance, owner):
        return self.val[instance]

    def __set__(self, instance, value):
        if value < 0:
            raise ValueError
        else:
            self.val[instance] = value


class BePositive:

    some_value = PositiveDescriptor('some')
    another_value = PositiveDescriptor('another')


q = BePositive()
inst = BePositive()
# должно работать
inst.some_value = 1
inst.another_value = 2
q.some_value = 3
q.another_value = 4
print(inst.another_value)
print(inst.some_value)
print(q.another_value)
print(q.some_value)
# должно выкинуть ошибку
inst.another_value = -1