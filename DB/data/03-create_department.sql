CREATE TABLE IF NOT EXISTS department (
    department_id BIGSERIAL PRIMARY KEY,
    department_name VARCHAR(255) UNIQUE,
    department_city INTEGER REFERENCES city(city_id)
);

INSERT INTO db_scheme_version(db_version, upgraded_on) VALUES ('1.2', now());