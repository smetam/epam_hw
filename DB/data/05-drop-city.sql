DROP TABLE city cascade;

ALTER TABLE employee
    ALTER COLUMN employee_city TYPE VARCHAR(255);

ALTER TABLE department
    ALTER COLUMN department_city TYPE VARCHAR(255);

INSERT INTO db_scheme_version(db_version, upgraded_on) VALUES ('1.4', now());
