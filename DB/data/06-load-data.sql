COPY department(department_name,department_city) FROM '/csv/DEPTS.csv' WITH DELIMITER ',' CSV HEADER;

COPY employee FROM '/csv/EMPLOYEE.csv' WITH DELIMITER ',' CSV HEADER;