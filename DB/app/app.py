import psycopg2
import time
import json as j


def get_cursor(attempts=0):
    try:
        conn = psycopg2.connect(host='db', port='5432',
                                user='postgres', password='asecurepassword')

        cursor = conn.cursor()
        return cursor
    except psycopg2.OperationalError as e:
        if attempts == 20:
            raise e
        else:
            time.sleep(3)
            return get_cursor(attempts + 1)


def query5(cursor):
    m = dict()
    n = dict()
    dif = dict()
    ask = 'SELECT employee_department, COUNT(employee_id) ' \
          'FROM employee GROUP BY employee_department;'
    cursor.execute(ask)
    result = cursor.fetchall()

    for i in result:
        cursor.execute(f"SELECT SUM(salary) FROM ( SELECT salary "
                       f"FROM employee WHERE employee_department = '{i[0]}' "
                       f"ORDER BY salary DESC LIMIT {int(i[1]/10)}) AS m")
        m[i[0]] = float(cursor.fetchall()[0][0])
        cursor.execute(f"SELECT SUM(salary) FROM ( SELECT salary "
                       f"FROM employee WHERE employee_department = '{i[0]}' "
                       f"ORDER BY salary ASC LIMIT {int(i[1]/10)}) AS n")
        n[i[0]] = float(cursor.fetchall()[0][0])

        # print(m[i[0]], n[i[0]])

        dif[i[0]] = m[i[0]] / n[i[0]]

    top1 = 0
    top1_dep = ""
    top2 = 0
    top2_dep = ""
    for key, val in dif.items():
        if val >= top1:
            top2 = top1
            top2_dep = top1_dep
            top1 = val
            top1_dep = key
        elif val > top2:
            top2 = val
            top2_dep = key

    return top1_dep, top2_dep


if __name__ == '__main__':
    res = dict()
    query = dict()

    query[1] = """SELECT name FROM (SELECT 
               (CONCAT(first_name, ' ', last_name)) AS name FROM employee) 
               AS names GROUP BY (name) ORDER BY COUNT(name) DESC  LIMIT 1;"""

    query[2] = 'SELECT COUNT(employee_id) FROM employee AS e ' \
               'INNER JOIN department AS d ' \
               'ON (e.employee_department = d.department_name) ' \
               'WHERE LOWER(e.employee_city) <> LOWER(d.department_city);'

    query[3] = 'SELECT COUNT(e.employee_id) FROM employee AS e ' \
               'INNER JOIN employee as b ' \
               'ON (e.boss = b.employee_id) ' \
               'WHERE e.salary > b.salary;'

    query[4] = 'SELECT employee_department FROM employee ' \
               'GROUP BY employee_department ' \
               'ORDER BY AVG(salary) DESC LIMIT 1;'
    
    cursor = get_cursor()

    for i in range(1, 5):
        cursor.execute(query[i])
        res[f'hw{i}'] = cursor.fetchall()[0][0]

    res['hw5'] = [x for x in query5(cursor)]
    r = j.dumps(res, sort_keys=True, indent=4)
    print(r)
