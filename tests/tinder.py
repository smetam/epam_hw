import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        # Init driver
        self.driver = webdriver.Chrome()  #executable_path='/chromedriver'

    def test_search_in_python_org(self):
        driver = self.driver
        # access tinder
        driver.get("https://tinder.com/")
        time.sleep(4)

        # login by phone number
        buttons = driver.find_elements_by_xpath('//*[@class="button__text '
                                                'Pos(r) Z(1) D(ib)"]/span')
        buttons[1].click()
        time.sleep(3)

        # enter number
        window = driver.find_element_by_xpath('//*[@name="phone_number"]')
        window.send_keys(input('Enter your phone number without +7'))

        # Confirm
        elements = driver.find_elements_by_xpath('//*[@class="button__text '
                                                 'Pos(r) Z(1)"]/span')
        elements[3].click()
        time.sleep(5)

        # enter SMS code
        window = driver.find_element_by_xpath(
            '//*[@class="Sq(48px) Sq(40px)--s Ta(c) Fz($m) Fw($bold) '
            'Mend(6px) Bdbc($c-secondary) Bdrs(4px) Bgc($c-divider-lite)"]')
        window.send_keys(input('Enter SMS code'))

        # Confirm
        elements = driver.find_elements_by_xpath(
            '//*[@class="button__text Pos(r) Z(1)"]/span')
        elements[3].click()
        time.sleep(5)

        # Skip 4 into pages
        skip = driver.find_element_by_xpath(
            '//*[@class="P(0) Py(25px) Px(35px)"]/button/span/span')
        skip.click()
        time.sleep(2)

        skip = driver.find_element_by_xpath(
            '//*[@class="P(0) Py(25px) Px(35px)"]/button/span/span')
        skip.click()
        time.sleep(2)

        skip = driver.find_element_by_xpath(
            '//*[@class="P(0) Py(25px) Px(35px)"]/button/span/span')
        skip.click()
        time.sleep(2)

        skip = driver.find_element_by_xpath(
            '//*[@class="P(0) Py(25px) Px(35px)"]/button[1]/span/span')
        skip.click()
        time.sleep(3)

        # start swiping
        actions = ActionChains(driver)
        time.sleep(2)
        for i in range(20):
            actions.send_keys(Keys.ARROW_RIGHT).perform()
            time.sleep(2)
            try:
                skip = driver.find_element_by_xpath('//*[@class="button Lts($ls-s) Z(0) Cur(p) Tt(u) Ell Bdrs(100px) Px(24px) Py(0) H(54px) Mih(54px) Lh(50px) button--outline Bdw(2px) Bds(s) Trsdu($fast) Bdc($c-gray) C($c-gray) Bdc($c-base):h C($c-base):h Fw($semibold) Bdc($c-pink) Bdc($c-orange):h C(#fff)!:h Bg(t):h W(100%) D(b) C(#fff) Bg(t) Mt(24px) Mt(12px)--xs Mt(10px)--lsh"]/span/span')
                skip.click()
                time.sleep(1)
            except Exception as e:
                #print(e)
                time.sleep(1)

        time.sleep(3)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
