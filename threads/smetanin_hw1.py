import psutil as psu


def process_count(username: str) -> int:
    # к-во процессов, запущенных из-под текущего пользователя
    pid_count = 0
    for pid in psu.process_iter():
        if pid.username() == username:
            pid_count += 1

    return pid_count


def total_memory_usage(root_pid: int) -> int:
    # суммарное потребление памяти древа процессов
    if root_pid in psu.pids():
        process = psu.Process(root_pid)
    else:
        return 0

    memory = process.memory_info()[0]
    for proc in process.children(recursive=True):
        memory += proc.memory_info()[0]
    return memory


print(process_count('b2w'))
print(total_memory_usage(835505))
