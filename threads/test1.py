import socket
import time

c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c.connect(('localhost', 8000))
while True:
    data = bytes(input("Enter data to send to server: "
                       "press q or Q to quit:\n"),
                 "utf-8")
    c.send(data)
    if data in ['q', 'Q']:
        c.close()
        break
    print(c.recv(512))
    time.sleep(3)
