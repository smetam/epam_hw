import signal
from socket import *
import hashlib
import os
import sys
import time


def prod_tree(l, r):
    if l > r:
        return 1
    elif l == r:
        return l
    elif r - l == 1:
        return l * r
    else:
        m = (l + r) // 2
        return prod_tree(l, m) * prod_tree(m + 1, r)


def fac_tree(n):
    if n < 0:
        return 0
    elif n == 0:
        return 1
    elif n in [1, 2]:
        return n
    else:
        return prod_tree(2, n)


def main_server_function(port: int = 8006, num_of_workers: int = 7):
    # :param port : port number to accept the incoming requests
    # :param num_of_workers : number of workers to handle the requests
    timing = {}
    s = socket(AF_INET, SOCK_STREAM)
    s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    s.bind(('localhost', port))
    s.listen(30)
    print(f"Server is running on port {str(port)} "
          f"with {num_of_workers} workers")
    m_pid = os.getpid()
    pids = []
    for i in range(num_of_workers):
        pid = os.fork()
        pids.append(pid)
        if pid == 0:
            childpid = os.getpid()
            print(f"Child {childpid} listening on localhost:{port}")
            try:
                while True:
                    conn, addr = s.accept()
                    print('conn')
                    conn.send(bytes('enter num', 'utf-8'))
                    data = conn.recv(1024).decode('utf-8')
                    print(data)
                    num = int(data)
                    f = fac_tree(num)
                    res = hashlib.md5(str(f).encode()).hexdigest()
                    print(res)
                    conn.send(bytes(res, 'utf-8'))
                    conn.close()
                # os.kill(os.getpid(), signal.SIGKILL)
            except KeyboardInterrupt:
                s.close()
                sys.exit()
            except Exception as e:
                s.close()
                raise e

    try:
        while True:
            time.sleep(2)
    except KeyboardInterrupt:
        for pid in pids:
            os.kill(pid, signal.SIGKILL)
        s.close()


if __name__ == "__main__":
    main_server_function()
