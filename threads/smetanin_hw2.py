import threading
import time
import signal


sem = threading.Semaphore()
CONT = True


def fun1():
    while CONT:
        sem.acquire()
        print(1)
        sem.release()
        time.sleep(1)


def fun2():
    while CONT:
        sem.acquire()
        print(2)
        sem.release()
        time.sleep(1)


def signal_handler(sig, frame):
    global CONT
    CONT = False


#sem.acquire()
t1 = threading.Thread(target=fun1)
t1.start()
t2 = threading.Thread(target=fun2)
t2.start()

while True:
    signal.signal(signal.SIGINT, signal_handler)
    time.sleep(0.5)
    if CONT is False:
        break
