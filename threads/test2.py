import socket
import time
import threading as thr


def get_hash(i):
    c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    c.connect(('localhost', 8006))
    msg = c.recv(1024).decode("utf8")
    print(msg)
    # data = bytes(input(), 'utf-8')
    data = bytes(str((i+1)*50000), 'utf-8')
    c.send(data)
    msg = c.recv(1024).decode("utf8")
    print(msg, '\n')
    c.close()
    return msg


number_of_workers = 30
t = time.time()
threads = {}
for i in range(number_of_workers):
    threads[i] = thr.Thread(target=get_hash, args=(0,))
    threads[i].start()

for i in range(number_of_workers):
    threads[i].join()
print(f"TOTAL: {time.time() - t}")
