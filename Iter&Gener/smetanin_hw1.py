"""
E - dict(<V> : [<V>, <V>, ...])
Ключ - строка, идентифицирующая вершину графа
значение - список вершин, достижимых из данной
"""


class Graph:
    def __init__(self, E):
        self.E = E


class GraphIterator:
    def __init__(self, g, start_v):
        self.graph = g
        self.current_pos = start_v
        self.stack = []
        self.marked = {}
        for v in g.E.keys():
            self.marked[v] = False
        self.marked[start_v] = True

    def __iter__(self):
        return self

    def hasNext(self) -> bool:
        if False in self.marked.values():
            return True
        else:
            return False

    def __next__(self) -> str:
        if self.current_pos is not None:
            d = self.current_pos
            for v in self.graph.E.get(d, []):
                if not self.marked[v]:
                    self.stack.append(v)
                    self.marked[v] = True
            self.graph.E.pop(d, None)
            if len(self.stack) > 0:
                self.current_pos = self.stack.pop()
            elif self.hasNext():
                key, val = self.graph.E.popitem()
                self.current_pos = key
                self.marked[key] = True
                for v in val:
                    self.stack.append(v)
                    self.marked[v] = True
            else:
                self.current_pos = None
            return d
        else:
            raise StopIteration


T = {'A': ['B', 'C', 'D'], 'B': ['C'], 'C': [], 'D': ['A']}
g0 = Graph(T)
E = {"A": ["B", "C"], "B": ["A", "C", "D", "E"], "C": ["A", "B", "E"], "D": ["B"], "E": ["B", "C"]}
start_v = "A"
g1 = Graph(E)


g2 = Graph({"A": ["B", "C"], "B": ["A", "C", "D", "E"], "C": ["A", "B", "E"],
            "D": ["B"], "E": ["B", "C"], "F": ["G"], "G": ["F"]})
