from pathlib import Path
from os import listdir, link
from os.path import isfile, join


def hardlink_check(directory_path: str) -> bool:
    try:
        path = Path(directory_path)
        if not path.exists():
            return False
        elif path.is_dir():
            files = [f for f in listdir(directory_path)
                     if isfile(join(directory_path, f))]

            inodes = set()
            for f in files:
                i = Path.stat(path / f).st_ino
                # print(f, i)
                if i in inodes:
                    return True
                else:
                    inodes.add(i)

        return False
    except Exception as e:
        return False


# print(hardlink_check(''))
# link('./test/test1.txt', './test/ts1.txt')
