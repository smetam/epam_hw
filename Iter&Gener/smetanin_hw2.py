def merge_files(fname1: str, fname2: str) -> str:
    f1 = open(fname1, 'r')
    f2 = open(fname2, 'r')
    res_file = open('res_file.txt', 'w')

    i1 = f1.readline()
    i2 = f2.readline()
    while i1 != '' and i2 != '':
        if int(i2) > int(i1):
            res_file.write(i1.strip() + '\n')
            i1 = f1.readline()
        else:
            res_file.write(i2.strip() + '\n')
            i2 = f2.readline()

    if i2 != '':
        res_file.write(i2.strip() + '\n')
        for i in f2:
            res_file.write(i.strip() + '\n')
    elif i1 != '':
        res_file.write(i1.strip() + '\n')
        for i in f1:
            res_file.write(i.strip() + '\n')

    res_file.close()

    return 'res_file.txt'
