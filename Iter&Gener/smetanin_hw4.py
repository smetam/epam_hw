from enum import Enum


class TokenType(Enum):
    VAR = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                't', 'u', 'v', 'w', 'x', 'y', 'z']
    BRACKET_OPEN = '('
    BRACKET_CLOSE = ')'
    OPERATION_PR1 = '/', '*'
    OPERATION_PR2 = '+', '-'


def to_pol(expr):
    expr = '(' + expr + ')'
    stack = []
    mas = ''
    for sym in expr:
        for tok in TokenType:
            if sym in tok.value:
                current_token = tok
        if current_token == TokenType.VAR:
            mas += sym
        elif current_token == TokenType.BRACKET_OPEN:
            stack.append(sym)
        elif current_token == TokenType.BRACKET_CLOSE:
            a = stack.pop()
            while a != '(':
                mas += a
                a = stack.pop()
        elif current_token == TokenType.OPERATION_PR1:
            a = stack.pop()
            while a in TokenType.OPERATION_PR1.value:
                mas += a
                a = stack.pop()
            stack.append(a)
            stack.append(sym)
        elif current_token == TokenType.OPERATION_PR2:
            a = stack.pop()
            while a != TokenType.BRACKET_OPEN.value:
                mas += a
                a = stack.pop()
            stack.append(a)
            stack.append(sym)
    return mas


def to_norm(expr):
    stack = []
    for sym in expr:
        for tok in TokenType:
            if sym in tok.value:
                current_token = tok
        if current_token == TokenType.VAR:
            stack.append((sym, TokenType.VAR))
        elif current_token == TokenType.OPERATION_PR1:
            s1, p1 = stack.pop()
            s2, p2 = stack.pop()
            if p2 == TokenType.OPERATION_PR2:
                s2 = '(' + s2 + ')'
            if p1 == TokenType.OPERATION_PR2:
                s1 = '(' + s1 + ')'
            elif sym == '/' and p1 != TokenType.VAR:
                s1 = '(' + s1 + ')'
            stack.append((s2 + sym + s1, TokenType.OPERATION_PR1))
        elif current_token == TokenType.OPERATION_PR2:
            s1, p1 = stack.pop()
            s2, p2 = stack.pop()
            if sym == '-' and p1 == TokenType.OPERATION_PR2:
                s1 = '(' + s1 + ')'
            stack.append((s2 + sym + s1, TokenType.OPERATION_PR2))
    s, p = stack.pop()
    return s


def brackets_trim(expr: str) -> str:
    return to_norm(to_pol(expr))


print(brackets_trim('(x+y)-(j*z)+g'))
print(to_pol('(x+y)/(j*z)+g '))