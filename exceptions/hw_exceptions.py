import os
import fcntl
import time


class pidfile:

    def __init__(self, path):
        self.path = path
        self.pidfile = None

    def __enter__(self):
        if self.path is None:
            return self.pidfile

        self.pidfile = open(self.path, "a+")
        try:
            fcntl.flock(self.pidfile.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            self.pidfile = None
            raise SystemExit("Already running according to " + self.path)
        return self.pidfile

    def __exit__(self, exc_type=None, exc_value=None, exc_tb=None):
        if self.pidfile is not None:
            self.pidfile.close()
            os.remove(self.path)


def test(a, b):
    print('Hello')

    time.sleep(1000)

    return a/b


if __name__ == '__main__':
    with pidfile('pid_file'):
        print('pid: ', os.getpid())
        test(1, 0)
