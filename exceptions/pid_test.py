from exceptions import hw_exceptions as hwe
import time
import os


if __name__ == '__main__':
    with hwe.pidfile('pid_file'):
        print('pid: ', os.getpid())
        hwe.test(1, 0)
