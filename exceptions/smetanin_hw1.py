import sys


def stderr_redirect(dest=None):

    def decorator(f):

        def wrapper(*args, **kwargs):
            stderr_keeper = sys.stderr
            sys.stderr = open(dest, 'a')
            res = f(*args, **kwargs)
            sys.stderr = stderr_keeper
            return res

        return wrapper

    return decorator


@stderr_redirect(dest='err2.txt')
def test2():
    sys.stderr.write('Trace from test2\n')


@stderr_redirect(dest='err1.txt')
def test1(a, b):
    test2()

    sys.stderr.write('Trace from test1\n')

    test2()


test1(1, 0)
