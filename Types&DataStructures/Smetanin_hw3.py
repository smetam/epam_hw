s = input()

while s != 'cancel':
    for i in s:
        if not (i.isdigit() or i == '-'):
            s = s.replace(i, ' ')

    s = s.replace('-', ' -')
    s = s.replace('- ', '')
    a = s.split()
    sum = 0
    for i in a:
        sum += int(i)

    print(sum)
    s = input()

print('Bye!')
