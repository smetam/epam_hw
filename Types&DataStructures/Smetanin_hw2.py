s = input().lower()
while s != 'cancel':
    w = s.split()
    d = dict()
    for i in w:
        if i in d:
            d[i] += 1
        else:
            d[i] = 1

    max = 0
    wordset = set()
    for key, value in d.items():
        if value > max:
            max = value
            wordset.clear()
            wordset.add(key)
        elif value == max:
            wordset.add(key)

    for i in wordset:
        print(max, ' - ', i)
    s = input().lower()

print('Bye!')

