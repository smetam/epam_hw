sum = 0
for i in range(1, 1000):
    str_num = str(i)
    num1 = int(str_num + str_num[-2::-1])
    num2 = int(str_num + str_num[::-1])
    bin1 = bin(num1)
    bin2 = bin(num2)
    if bin1[2:] == bin1[:1:-1]:
        sum += num1 
        print(num1, bin1)
    if bin2[2:] == bin2[:1:-1]:
        sum += num2
        print(num2, bin2)
print(sum)