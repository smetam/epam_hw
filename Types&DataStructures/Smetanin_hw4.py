s = input()

while s != 'cancel':
    a = set([0])
    # первый проход по списку длиной n - O(n)
    for i in s.split():
        a.add(int(i))

    # второй проход по списку длиной n - O(n)
    for j in range(len(a) + 1):
        if j not in a:
            print(j)
            break

    s = input()

print('Bye!')
# всего 2 раза перебрать список длиной n - алгоритм работает за линейное время